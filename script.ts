function hello() {
    console.log("Hello World");
}

// hello();

let myNumber = 1;
// myNumber = "hello";

hello();

let myNum: number;
let myString: string;
let myBoolean: boolean;
let unknownVar: any;
let myArray: number[] = [1, 2, 3];
let otherArray: any[] = ["John", 31, true];
myString = "hello";

// console.log("Result of annotations:")
// console.log(typeof myString);
// console.log(typeof myArray);

// console.log(typeof myArray);

function sayMyName(name: string) {
    return 1;
}

// console.log("Result of function annotations:");

// sayMyName(1);

let myName = sayMyName("John");

// console.log(myName);


function genericFunction <T>(param: T) {
    return param;
}

// console.log("Result of generics:");
let result = genericFunction("cooper");

// console.log(result);


result = genericFunction("cooper");

type numArray = Array<number>;
let myNumArray: numArray = [1, 2, 3];

// const user = {
//     name: "John Smith",
//     age: "31"
// }

const user: User = {
    name: "John Smith",
    age: 31
};

let addedAge = 10 + user.age;
console.log("In ten years your age would be: " + addedAge);

// interface User {
//     name: string;
//     age: number;
//     address?: string;
// };


type User = {
    name: string;
    age: number;
    address?: string;
};

function printUser(): User {
    let myUser: User = {
        name: "Jane",
        age: 18
    }

    return myUser;
}

// console.log("Result of functions with interfaces and types:");
// console.log(printUser());


// console.log("In ten years your age would be: " + addedAge);

type message = string | number;
let myMessage : message = "Hello Again";
// console.log("Result of type alias: " + myMessage);

// console.log("In ten years your age would be: " + addedAge);

let users: Array<User> = [
{
    name: "Joe",
    age: 25
}
]

console.log("Result from generics with interfaces and types:");
console.log(users);


console.log(users);


// let myMessage : message = 12;
// console.log("Result of type alias: " + myMessage);

type LockState = "locked" | "unlocked";
let myLock: LockState = "locked";

type favoriteNumbers = 24 | 11 | 29;
let myFavNumbers: favoriteNumbers = 11;